import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Search {
    private final WebDriver driver;

    public Search(WebDriver driver) {
        this.driver = driver;
    }

    public void searchFor(String text) {
        WebElement filterButton = driver.findElement(By.id("searchDropdownBox"));
        WebElement booksOption = driver.findElement(By.xpath("//*[@id='searchDropdownBox']/option[6]"));
        WebElement textField = driver.findElement(By.id("twotabsearchtextbox"));
        WebElement submitButton = driver.findElement(By.xpath("//*[@id='nav-search']/form/div[2]/div/input"));
        filterButton.click();
        booksOption.click();
        textField.sendKeys(text);
        submitButton.click();
    }
}
