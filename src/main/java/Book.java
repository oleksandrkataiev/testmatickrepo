import java.util.Objects;

public class Book {
    private final String title;
    private final String author;
    private final boolean bestSeller;

    public Book(String title, String author, boolean bestSeller) {
        this.title = title;
        this.author = author;
        this.bestSeller = bestSeller;
    }

    public String getName() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public boolean isBestSeller() {
        return bestSeller;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return bestSeller == book.bestSeller &&
                title.equals(book.title) &&
                author.equals(book.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, bestSeller);
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", isBestSeller=" + bestSeller +
                '}';
    }
}
