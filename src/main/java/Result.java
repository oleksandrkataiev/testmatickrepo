import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class Result {

    private WebDriver driver;

    public Result(WebDriver driver) {
        this.driver = driver;
    }

    public List<Book> getResultFromSearch() {
        List<Book> resultList = new ArrayList<>();
        List<WebElement> elements = driver.findElements(By.xpath("//div[@data-component-type='s-search-result']"));
        for (int i = 1; i <= elements.size(); i++) {
            String title = getTitle(i);
            String author = getAuthor(i);
            boolean isBestSeller = isBestSeller(i);
            resultList.add(new Book(title, author, isBestSeller));
        }
        return resultList;
    }

    private String getTitle(int i) {
        return driver.findElement(By.xpath("//div[@data-component-type='s-search-result'][" + i + "]//h2//span")).getText();
    }

    private String getAuthor(int i) {
        return driver.findElement(By.xpath("//div[@data-component-type='s-search-result'][" + i + "]//h2/../div/*[2]")).getText();
    }

    private boolean isBestSeller(int i) {
        boolean isBestSeller;
        try {
            isBestSeller = driver.findElement(By.xpath("//div[@data-component-type='s-search-result'][" + i + "]//span[contains(@id, 'best-seller-label')]")).isEnabled();
        } catch (Exception noSuchElement) {
            isBestSeller = false;
        }
        return isBestSeller;
    }
}
