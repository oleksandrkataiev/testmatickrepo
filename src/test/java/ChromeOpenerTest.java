import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.List;

public class ChromeOpenerTest {

    private InitDriver initDriver = new InitDriver();

    public ChromeOpenerTest() throws MalformedURLException {
    }

    @BeforeSuite
    public void setUp() {
        initDriver.goUrl("https://www.amazon.com");
    }

    @AfterMethod
    public void tearDown() {
        initDriver.getDriver().close();
    }

    @Test
    public void testMain() {
        Search amazon = new Search(initDriver.getDriver());
        amazon.searchFor(System.getProperty("search", "Java"));
        Result resultSearch = new Result(initDriver.getDriver());
        List<Book> amazonBooks = resultSearch.getResultFromSearch();
        Book expectedBook = expectedBook();
        Assert.assertTrue(amazonBooks.contains(expectedBook));
    }

    private Book expectedBook() {
        initDriver.goUrl("https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208/");
        WebDriver driver = initDriver.getDriver();
        String title = driver.findElement(By.id("productTitle")).getText();
        String author = driver.findElement(By.xpath("//*[@id='bylineInfo']/span[1]/span[1]/a[1]")).getText();
        boolean bestSeller;
        try {
            bestSeller = driver.findElement(By.xpath("//*[@id='zeitgeistBadge_feature_div']/div/a")).isEnabled();
        } catch (Exception noSuchElement) {
            bestSeller = false;
        }

        return new Book(title, author, bestSeller);
    }
}